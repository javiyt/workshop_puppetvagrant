define workshop::resources::vhost
(
	$www_root = '/vagrant',
	$pool_port = 9000,
)
{
	$vhost_dir = "/etc/nginx/sites-available/"
	$vhost_links = "/etc/nginx/sites-enabled/"

	$php_fpm_pool_dir = "/etc/php5/fpm/pool.d/"
	$php_fpm_pool_file = "$php_fpm_pool_dir$name.conf"

	$vhost_file = "$vhost_dir$name.vhost"
	$vhost_link = "$vhost_links$name.vhost"

	file {
		$vhost_dir:
			ensure 	=> directory,
			purge	=> true,
			recurse => true,
			require	=> Package['nginx'],
	}

	file {
		$vhost_links:
			ensure 	=> directory,
			purge	=> true,
			recurse => true,
			require	=> Package['nginx'],
	}

	file {
		$vhost_file:
			ensure	=> present,
			content	=> template('workshop/vhost.conf.erb'),
			require	=> File[$vhost_dir],
			notify	=> Service['nginx'],
	}

	file {
		$vhost_link:
			ensure	=> link,
			target	=> $vhost_file,
			require	=> File[$vhost_file],
			notify	=> Service['nginx'],
	}

	file {
		$php_fpm_pool_dir:
			ensure 	=> directory,
			purge	=> true,
			recurse => true,
			require	=> Package['php5-fpm'],
	}

	file {
		$php_fpm_pool_file:
			ensure 	=> present,
			content => template('workshop/php_pool.conf.erb'),
			notify	=> Service['php5-fpm'],
	}
}