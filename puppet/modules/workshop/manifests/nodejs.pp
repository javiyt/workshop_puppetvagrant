class workshop::nodejs
{
	$nodeVersion = '0.10.22'
	$nodeUrl = 'https://www.dropbox.com/s/ljlu1whpnogzb6c/nodejs_0.10.22-1_amd64.deb'
	$nodeDeb = "nodejs_$nodeVersion-1_amd64.deb"
	$tmp_path = '/tmp'

	exec {
		'download_node':
			command	=> "wget $nodeUrl",
			cwd			=> $tmp_path,
			creates		=> "$tmp_path/$nodeDeb",
			logoutput	=> false,
			onlyif		=> "dpkg -l nodejs | grep $nodeVersion-1 | wc -l",
	}

	package {
		'nodejs':
			ensure		=> latest,
			source		=> "$tmp_path/$nodeDeb",
			require		=> Exec['download_node'],
			provider	=> 'dpkg',
	}

	package {
		'coffee-script':
			ensure		=> absent,
			require		=> Package['nodejs'],
			provider	=> 'npm',
	}
}