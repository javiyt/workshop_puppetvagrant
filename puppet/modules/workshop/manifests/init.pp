class workshop()
{

	group {
	    'puppet':
	        ensure => present,
	}

	include workshop::repos
	include workshop::nginx
	include workshop::mongodb
	include workshop::nodejs

	class {
		'workshop::php':
			require => Class['workshop::nginx'],
	}

	Exec['apt_update'] -> Package <| |>
}