class workshop::nginx
{
	package {
		'nginx':
			name 	=> 'nginx-full',
			ensure 	=> present,
			require => Apt::Source['dotdeb'],
	}

	service {
		'nginx':
		    enable 		=> true,
			ensure 		=> running,
			hasrestart 	=> true,
			hasstatus 	=> true,
			require 	=> Package['nginx-full'],
	}
}