class workshop::mongodb
{
	package {
		'mongodb':
			ensure	=> installed,
			name 	=> 'mongodb-10gen',
	}

	service {
		'mongodb':
		    enable 		=> true,
			ensure 		=> running,
			hasrestart 	=> true,
			hasstatus 	=> true,
			require 	=> Package['mongodb'],
	}
}