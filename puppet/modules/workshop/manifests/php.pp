class workshop::php
{
	$packages = ['php5', 'php5-dev', 'php5-fpm', 'php5-cli']

	package {
		$packages:
			ensure => installed,
	}

	package { 'php-mongo':
		name		=> 'mongo',
		ensure 		=> installed,
		provider	=> 'pecl',
	}

	service {
		'php5-fpm':
		    enable 		=> true,
			ensure 		=> running,
			hasrestart 	=> true,
			hasstatus 	=> true,
			require 	=> Package['php5-fpm'],
	}

	file {
		'mongo_ini':
			ensure	=> present,
			path	=> '/etc/php5/mods-available/mongo.ini',
			source	=> 'puppet:///modules/workshop/mongo.ini',
			require	=> [Package['php5'], Package['php-mongo']],
	}

	file {
		'mongo_ini_fpm':
			path	=> '/etc/php5/fpm/conf.d/25-mongo.ini',
			ensure	=> link,
			target	=> '/etc/php5/mods-available/mongo.ini',
			require	=> File['mongo_ini'],
			notify	=> Service['php5-fpm'],
	}
}