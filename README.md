# Workshop de Vagrant + Puppet

## First exercise:
* Open the terminal:
	+ vagrant box add DebianWheezy debianwheezy.box
	+ vagrant init DebianWheezy
* Open the Vagrantfile generated and remove the comments not needed.
* Go to the terminal again:
	+ vagrant up
	+ vagrant ssh

In case of failing remove the machine and start it again (vagrant destroy -f)

## Second exercise:
* Adding a hostname to the machine:
	+ Open Vagrantfile and add: config.vm.hostname = "development.workshop.dev"
	+ vagrant reload
	+ vagrant ssh
	+ hostname -a

## Third exercise:
* Create a default.pp file to start using puppet:
	+ Uncomment puppet configuration and adjust it
	+ Add default.pp file with basic statements
	+ vagrant reload
	+ vagrant provision

## Fourth exercise:
* Creating a module:
	+ Create modules folder, and whole structure
	+ Move code from default.pp to module init.pp file
	+ Call the module from the default.pp file
	+ Add the modules path to the Vagrantfile
	+ vagrant reload
	+ vagrant provision

## Fifth exercise:
* Add the apt module downloaded from (forge.puppetlabs.com)
	+ Go to https://github.com/puppetlabs/puppetlabs-apt/releases and download the latest version from the module
	+ Uncompress it on your modules folder
	+ Download the stdlib module (it is a dependency), go to https://github.com/puppetlabs/puppetlabs-stdlib/releases
	+ Uncompress it on your modules folder
	+ Create a class in the module to manage the repositories needed
	+ Call the class from your module
	+ vagrant provision

## Sixth exercise:
* Install nginx on your machine
	+ Add the dotdeb repository to your sources
	+ Create a class to install nginx
		- Install the package (remember it is required to add dotdeb repository before install the package)
		- Create the service to ensure is running
	+ Add the option to execute apt_update after package installation
	+ Call the class
	+ vagrant provision
	+ Go to http://localhost:8080 nginx should be running

## Seventh exercise
* Install PHP and PEAR packages:
	+ Create a class to install PHP 5.5 and all its needed packages
	+ Call the PHP class from the main module file, remember to add a dependency between php and nginx
	+ vagrant ssh
	+ Check PHP is installed
	+ Add the PECL provider
	+ Install mongo package using PECL provider
	+ Add node file for development machine
	+ Add vhost to check Nginx + PHP are running

## Eighth exercise
* Install MongoDB from MongoDB Inc repositories
	+ Create a class to manage MongoDB
	+ Enable MongoDB Inc repository
	+ Add class to main file of the module
	+ Check MongoDB is working

## Extra exercises
* Install Node.js, and npm package provider
* Write a small page to connect to MongoDB and show data from database (to ensure you have a server up and running)